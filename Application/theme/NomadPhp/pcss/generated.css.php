{"hash":"7b0439dad561e1dd5b928f0d1bd2efcd"}
<?php
/**
 * Always start a .css.php file on line 2 because the above hash is generated just to detect changes.
 * The {filename}.css.php will generate a {filename}.css file in the {themeName}/assets/css/ directory. This file
 * will then be copied to the public folder along with the other assets; but only for `development` environments.
 *
 * This is an example file. Here you can use any Xcss functions; e.g, $color->shade(), $asset->image() etc.
 */
?>

<?php
	$mainColor = "#00BBFF";
	$secondaryColor = "#00A8B2";
	$tertiaryColor = "#BF3E0A";
?>
body{
	background: <?= $asset->image('nomadbw.jpg');?> no-repeat;
	background-size: cover;
	background-position: bottom ;
	font-family: Tahoma, Geneva, sans-serif;
	min-width:100%;
	min-height:100vh;
}
footer{
	background: linear-gradient(to right, rgba(255,255,255, .33), rgba(0,0,0,0));
	color: #efefef;
	position: fixed;
	bottom:0;
}
nav {
/*	background: linear-gradient(to right, */<?//=$secondaryColor?>/* 25%, rgba(0,0,0,0) 33%);*/
}
header, nav{
	color: #ffffff;
}
h1, h1 a{
	color: <?= $mainColor?>;
}
nav a{
	color: #ffffff;
}