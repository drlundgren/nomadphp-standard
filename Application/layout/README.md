# ./Application/layout directory
This directory contains layouts that can be used within controllers (see annotations). Its useful when you want to drastically 
change the layout of a theme without creating an entire new theme. 