<?php
namespace Application\Controller;

class Index extends \Nomad\Core\Controller
{
	/**
	 * @Nomad\Layout singleColumn
	 */
	public function index()
	{
		/**
		 * Examples of how to set variables into the view. Variables can be of any type.
		 */
		$this->view->freeMessage = "You may use NomadPhp framework freely.";
		$this->view->freeList = array(
			"No payments required", "No hidden fees", "No subscriptions", "Cancel anytime"
		);

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function about()
	{
		$this->view->hello = "Hello";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function docs()
	{
		$this->view->hello = "Hello";

	}

	/**
	 * @Nomad\Layout singleColumn
	 */
	public function showcase()
	{
		$this->view->freeMessage = "Hello";

	}
}